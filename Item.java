package assessment;

public class Item 
{
    private String m_Name;
    private float m_Price;
    private int m_Quantity;
    
    public Item(String name, float price, int quantity)
    {
        m_Name = name;
        m_Price = 1.0f + price;
        m_Quantity = quantity;
    }
    
    public String getName()
    {
        return m_Name;
    }
    public float getPrice()
    {
        return m_Price;
    }
    public int getQuantity()
    {
        return m_Quantity;
    }
    public void decrementQuantity()
    {
        --m_Quantity;
    }
}
