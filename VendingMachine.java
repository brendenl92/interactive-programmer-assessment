package assessment;

import java.util.Scanner;
import java.util.Random;
import java.text.DecimalFormat;

        
public class VendingMachine 
{
    
    private Item[] item;
    
    public void run()
    {
        Scanner scan = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("####0.00");
        fillMachine();
        char option = ' ';
        while(option != 'N')
        {
            System.out.println("Would you like to see the list of snacks? Y/N");
            option = scan.next().toUpperCase().charAt(0);
            if(option != 'N')
            {
                System.out.println("Choices are: ");
                for(int i = 0; i < 5; i++)
                {
                    System.out.println(i + " - " + item[i].getName() + " || $" + df.format( item[i].getPrice()));
                }
                System.out.println("Please insert money to make a purchase, or N to quit");
                if(scan.hasNextDouble())
                {
                    String moneyOrQuit = scan.next();
                    double money = Double.parseDouble(moneyOrQuit);
                    System.out.println(money + " has been inserted, please make a selection.");
                    boolean goodSelection = false;
                    while(goodSelection == false)
                    {
                        while(!Character.isDigit(option))
                        {
                            option = scan.next().charAt(0);
                            if(!Character.isDigit(option))
                                System.out.println("Invalid selection, try again.");
                        }
                        if(item[Character.getNumericValue(option)].getQuantity() > 0)
                            goodSelection = true;
                        else
                        {
                            System.out.println(item[Character.getNumericValue(option)].getName() + " is sold out, please make another selection");
                            option = scan.next().charAt(0);
                            if(Character.isDigit(option) == false)
                            {
                                
                            }
                        }
                    }
                    double difference = money - item[Character.getNumericValue(option)].getPrice();
                    if(difference < 0.0)
                    {
                        boolean costFulfilled = false;
                        while(!costFulfilled)
                        {
                            System.out.println("Not enough money inserted, $" + df.format(Math.abs(difference)) + " is remaining.");
                            System.out.println("Please insert more money...");
                            if(scan.hasNextDouble() == true)
                            {
                                double moreMoney = scan.nextDouble();
                                difference += moreMoney;
                                if(difference > 0.0)
                                    costFulfilled = true;
                            }
                        }
                    }
                    item[Character.getNumericValue(option)].decrementQuantity();
                    System.out.println("Item dispensed, your change is: " + df.format(difference) + ". Thank you!");
                }
                else
                {
                    option = 'N';
                }
            }
        }     
    }
    
    private void fillMachine()
    {
        Random ran = new Random(25565);
        item = new Item[5];
        item[0] = new Item("Coke", ran.nextFloat(), ran.nextInt(15)); 
        item[1] = new Item("Sprite", ran.nextFloat(), ran.nextInt(15));
        item[2] = new Item("Rootbeer", ran.nextFloat(), ran.nextInt(15));
        item[3] = new Item("Water", ran.nextFloat(), ran.nextInt(15));
        item[4] = new Item("Fanta", ran.nextFloat(), ran.nextInt(15));
    }
}